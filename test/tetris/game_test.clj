(ns tetris.game-test
  (:require [clojure.test :refer :all]
            [tetris.core :refer :all]
            [tetris.game :as g])
  (:require [tetris.test-data :as td]))

(deftest simple-next-piece
  (let [game (g/make "Q0,Z2" nil)]
    (testing "simple next piece works")
    (is (= td/Q0 (g/next-piece game) ))
    (is (= td/Z2 (g/next-piece (g/++ game))))))
