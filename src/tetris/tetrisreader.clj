(ns tetris.tetrisreader (:gen-class)
  (:use clojure.java.io))

(defrecord TetrisReader [lines])

(defn make [filename] (TetrisReader. (line-seq (reader filename))))

(defn ++ [tetrisreader] (TetrisReader. (rest (:lines tetrisreader))))

(defn eof? [tetrisreader] (empty? (:lines tetrisreader)))

(defn current [tetrisreader]
  (if-let [eof (not (eof? tetrisreader))]
    (first (:lines tetrisreader))))
