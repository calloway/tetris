(ns tetris.ui.console  
  (:require [tetris.gameptr :as gp])
  (:require [tetris.playgame :as pg]))

(defn to-string [ls]
  (str " |" (clojure.string/join ls) "|"))

(defn convert-row [ls i numcols]
  (when-not (= i numcols) 
    (if (= i (first ls))
      (cons "*" (convert-row (rest ls) (inc i) numcols))
      (cons " " (convert-row ls (inc i) numcols)))))

(defn update-grid-fn [grid & [finalheight]]
  (do
    (println "")
    (when-not (nil? finalheight) (println "Final Height " finalheight))
    (println "0" (clojure.string/join (repeat (:numcols grid) "-")))
    (loop [f (map #(convert-row % 0 (:numcols grid)) (:occupied grid))]
      (if (not (empty? f))
        (do
          (println (to-string (first f)))
          (recur (rest f)))))))
 
(defn -main [gameptr]
  (when-not (nil?  gameptr))
      (println (str "Playing Game " (:turns (:currentgame gameptr))))
      (pg/play-game gameptr (fn [grid & finalheight] (update-grid-fn grid finalheight)))
      (recur (gp/++ gameptr)))
