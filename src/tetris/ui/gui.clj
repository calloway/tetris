(ns tetris.ui.gui  
  (:require [tetris.gameptr :as gp])
  (:require [tetris.playgame :as pg])
  (:import (tetris.ui.TetrisPanel))
  (:import (javax.swing JFrame SwingUtilities)))

(defn- tetris-frame [panel]
  (let [frame (JFrame. "Tetris")]
    (doto frame
      (.add panel)
      (.pack)
      (.setVisible true)
      (.setDefaultCloseOperation JFrame/DISPOSE_ON_CLOSE))))
  
(defn -main [gameptr]
  (let [grid (:grid (:currentgame gameptr))
        input (clojure.string/join (:turns (:currentgame gameptr)))
        panel (tetris.ui.TetrisPanel. (:numcols grid) (:numrows grid) input)
        frame (tetris-frame panel)
        update-grid-fn (fn [grid & [finalheight]]
                         (do
                           (when-not (nil? finalheight) (.setfinalheight panel finalheight))
                           (.setgrid panel grid)))] 
    (when-not (nil? gameptr)
      (pg/play-game gameptr update-grid-fn)                      
      (recur (gp/++ gameptr)))))
