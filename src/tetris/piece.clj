(ns tetris.piece)

(defrecord Piece [shape leftmost topmost])

(defrecord Shape [pattern])

;;; shapes are defined as list of rows
(def Q (Shape. '((0 1) (0 1))))
(def Z (Shape. '((0 1) (1 2))))
(def T (Shape. '((0 1 2) (1))))
(def I (Shape. '((0 1 2 3))))
(def L (Shape. '((0) (0) (0 1))))
(def J (Shape. '((1) (1) (0 1))))
(def S (Shape. '((1 2) (0 1))))

(defn make [raw]
  (let [shape-raw (str (first raw))
        leftmost-raw (first (rest raw))
        to-int #(- (int %) 48)]
    (cond
     (= shape-raw "Q" ) (Piece. Q (to-int leftmost-raw) 0)
     (= shape-raw "Z" ) (Piece. Z (to-int leftmost-raw) 0)
     (= shape-raw "T" ) (Piece. T (to-int leftmost-raw) 0)
     (= shape-raw "I" ) (Piece. I (to-int leftmost-raw) 0)
     (= shape-raw "L" ) (Piece. L (to-int leftmost-raw) 0)
     (= shape-raw "J" ) (Piece. J (to-int leftmost-raw) 0)
     (= shape-raw "S" ) (Piece. S (to-int leftmost-raw) 0) 
     :else nil)))

(defn fall [piece]
  (Piece. (:shape piece) (:leftmost piece) (inc (:topmost piece))))

(defn occupying [piece]
  (if (= 0 (:topmost piece))
    (map (fn [nested-ls] (map (fn [col] (+ col (:leftmost piece))) nested-ls))
         (:pattern (:shape piece)))
    (cons '() (occupying (Piece. (:shape piece) (:leftmost piece)
                                 (dec (:topmost piece)))))))
