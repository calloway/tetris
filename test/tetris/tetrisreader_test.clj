(ns tetris.tetrisreader-test
  (:require [clojure.test :refer :all]
            [tetris.core :refer :all]
            [tetris.tetrisreader :as tr]
            [tetris.grid :as gr]
            [tetris.game :as g]
            [tetris.piece :as p])
    (:require [tetris.test-data :as td]))

(deftest eof-reached
  (testing "eof reached"
    (let [reader (tetris.tetrisreader.TetrisReader. ["only line"])]
      (is (false? (tr/eof? reader)))
      (is (true? (tr/eof? (tr/++ reader)))))))
