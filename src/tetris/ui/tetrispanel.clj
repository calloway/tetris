(ns tetris.ui.tetrispanel
  (:require [tetris.grid :as gr])
  (:import (javax.swing JPanel))
  (:import (java.awt Color Dimension Font))
  (:gen-class
   :extends javax.swing.JPanel
   :name tetris.ui.TetrisPanel
   :state state
   :init init
   :constructors {[int int String] []}
   :methods [[setgrid [tetris.grid.Grid] void]
             [setinput [String] void]
             [setfinalheight[int] void]]  
   :exposes-methods {paintComponent superPaintComponent}))

(def CELLWIDTH 30)

(defn -init
  [numcols numrows input]
  [[] (atom {:grid (gr/make numcols numrows) :input input :finalheight nil})])

(defn- draw-grid-squares [this grid graphics]
  (doall
   (for [col (range (:numcols grid)) row (range (:numrows grid))]
     (let [xpos (fn[ls] (first ls))
           ypos (fn[ls] (first (rest ls)))
           northwest (list (* col CELLWIDTH) (* row CELLWIDTH))
           southwest (list (xpos northwest) (+ CELLWIDTH (* row CELLWIDTH)))
           northeast (list (+ CELLWIDTH (* col CELLWIDTH)) (ypos northwest) )
           southeast (list (xpos northeast) (ypos southwest))]
       (doto graphics
         (.setColor Color/WHITE)
         (.drawLine (xpos northeast) (ypos northeast) (xpos southeast) (ypos southeast))
         (.drawLine (xpos southwest) (ypos southwest) (xpos southeast) (ypos southeast)))))))

(defn- draw-grid[this grid graphics]
  (doall (for [col (range (:numcols grid)) row (range (:numrows grid))]
           (if (gr/occupied? grid col row)
             (doto graphics
               (.setColor Color/GREEN)
               (.fillRect (* col CELLWIDTH) (* row CELLWIDTH) CELLWIDTH CELLWIDTH))))))

(defn- getinput [this]
  (:input @(.state this)))

(defn- getfinalheight [this]
  (:finalheight @(.state this)))

(defn- getgrid [this]
  (:grid @(.state this)))
  
(defn -setgrid [this grid]
  (do (reset! (.state this) {:grid grid :input (getinput this) :finalheight (getfinalheight this)})
      (.repaint this)))

(defn -setinput [this input]
  (do (reset! (.state this) {:grid (getgrid this) :input input :finalheight (getfinalheight this)})
      (.repaint this)))

(defn -setfinalheight [this finalheight]
  (do (reset! (.state this) {:grid (getgrid this) :input (getinput this) :finalheight finalheight})
      (.repaint this)))

(defn- show-messages [this graphics]
  (doto graphics
    (.setColor Color/RED)
    (.setFont (Font. "Serif" Font/BOLD 14))
    (.drawString (str "INPUT: " (getinput this)) 10 200)
    (.drawString (str "FINAL HEIGHT: " (getfinalheight this)) 10 240)))
    
(defn -paintComponent [this graphics]
  (.superPaintComponent this graphics)
  (draw-grid this (getgrid this) graphics)
  (draw-grid-squares this (getgrid this) graphics)
  (show-messages this graphics))

(defn -getBackground [this] (Color/BLACK))

(defn -getPreferredSize [this]
  (Dimension. (* CELLWIDTH gr/MAXCOLS) (* CELLWIDTH gr/MAXROWS )))
