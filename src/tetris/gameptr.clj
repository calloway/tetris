(ns tetris.gameptr
  (:require [tetris.tetrisreader :as tr])
  (:require [tetris.game :as g])
  (:require [tetris.grid :as gr]))

(defrecord GamePtr [tetrisreader currentgame])

(defn make [tetrisreader grid]
  (GamePtr. tetrisreader
            (g/make (tr/current tetrisreader) grid)))

(defn update-with-grid [gameptr grid]
  (let [game (:currentgame gameptr)]
    (GamePtr. (:tetrisreader gameptr)
              (tetris.game.Game. (:turns game) (:i game) grid))))  

(defn inc-turn [gameptr]
  (GamePtr. (:tetrisreader gameptr)
            (g/++ (:currentgame gameptr))))

(defn ++ [gameptr]
  (let [next-reader (tr/++ (:tetrisreader gameptr))]
    (if-not (tr/eof? next-reader)
      (make next-reader
            (gr/make (:numcols (:grid (:currentgame gameptr)))
                     (:numrows (:grid (:currentgame gameptr))))))))
