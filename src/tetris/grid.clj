(ns tetris.grid (:gen-class)
  (:require [tetris.piece :as p]))

(def MAXCOLS 10)
(def MAXROWS 20)

(defrecord Grid [numcols numrows occupied])

(defn make [cols rows] (Grid. cols rows (take rows (repeat '()))))

(defn highest-occupied-rowcount [grid]
  (loop [occ (:occupied grid)]
    (if (or (nil? (first occ))
            (not (empty? (first occ))))
      (count occ)
      (recur (rest occ)))))

(defn occupied? [grid, x, y]
  (let [horc (highest-occupied-rowcount grid)
        row-at-y (if (< y (count (:occupied grid))) (nth (:occupied grid) y))
        y-on-grid (- (:numrows grid) y)
        list-contains #(not (nil? (some #{%1} %2 )))]
    (if (< horc y-on-grid)
      false
      (list-contains x row-at-y))))

(defn- fallable-helper [occupying grid row]
  (if (empty? occupying)
    true
    (and (true? (every? #(not (occupied? grid %1 row)) (first occupying)))
         (fallable-helper (rest occupying) grid (inc row)))))
    
(defn fallable? [grid, piece]
  (let [occupying (p/occupying (p/fall piece))]
    (if (> (count occupying) (:numrows grid))
      false
      (true? (fallable-helper occupying grid 0)))))

(defn occupying-merge [o1 o2]
  (cond
   (empty? o1) o2
   (empty? o2) o1
   :else
   (cons (sort (distinct (concat (first o1) (first o2)))) 
         (occupying-merge (rest o1) (rest o2)))))
          
(defn occupy-grid-with-piece [grid, piece]
  (if (nil? piece)
    grid
    (Grid. (:numcols grid) (:numrows grid)
           (occupying-merge (:occupied grid)
                            (p/occupying piece)))))

;; Collapse rows that span the columns.  Just like tetris 
(defn pack-grid-helper [occupied grid rowsvisited]
  (if-let [finished? (= rowsvisited (:numrows grid))]
    occupied
    (if-let [ 
             packable? (= (count (last occupied)) (:numcols grid))]
      (pack-grid-helper (cons '() (butlast occupied)) grid (inc rowsvisited))
      (concat (pack-grid-helper (butlast occupied) grid (inc rowsvisited)) (list (last occupied))))))

(defn pack-grid [grid]
  (Grid. (:numcols grid) (:numrows grid) (pack-grid-helper (:occupied grid) grid 0)))
