(ns tetris.piece-test
  (:require [clojure.test :refer :all]
            [tetris.core :refer :all]
            [tetris.tetrisreader :as tr]
            [tetris.grid :as gr]
            [tetris.game :as g]
            [tetris.piece :as p])
    (:require [tetris.test-data :as td]))

(deftest simple-fall
  (testing "simple fall of piece")
  (is (= (p/fall td/PieceZ50) td/PieceZ51)))

(deftest simple-piece-occupy
  (testing "simple piece occupy")
  (is  (= '(() () () () () () () () (0 1) (0 1)) (p/occupying td/PieceQ08)))
  (is  (= '(() () () () () () () () (2 3) (3 4)) (p/occupying td/PieceZ28))))

(deftest simple-piece-make
  (testing "simple piece make works")
  (is (= 0 (:leftmost td/PieceQ00)))
  (is (= 9 (:leftmost td/PieceQ90)))
  (is (= p/Q (:shape td/PieceQ00))))
