(ns tetris.game
  (:require [tetris.piece :as p])
  (:require [tetris.grid :as gr]))

;; Game stores list of raw input, current index into list, and the grid
(defrecord Game [turns i grid])

(defn make [raw grid] (Game. (clojure.string/split raw #",") 0 grid))

;; Gets next piece based on i of game
(defn next-piece [game]
  (if-not (>= (:i game) (count (:turns game))) 
    (p/make (nth (:turns game) (:i game)))))

;; Increments the i to next turn
(defn ++ [game]
  (Game. (:turns game) (inc (:i game)) (:grid game )))
