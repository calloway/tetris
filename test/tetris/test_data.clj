(ns tetris.test-data
  (:require [clojure.test :refer :all]
            [tetris.core :refer :all]
            [tetris.grid :as gr]
            [tetris.game :as g]
            [tetris.piece :as p]))

(def Q0 (p/make "Q0"))
(def Z2 (p/make "Z2"))

(def PieceQ00 (tetris.piece.Piece. p/Q 0 0))
(def PieceQ01 (tetris.piece.Piece. p/Q 0 1))
(def PieceQ03 (tetris.piece.Piece. p/Q 0 3))
(def PieceQ08 (tetris.piece.Piece. p/Q 0 8))
(def PieceQ11 (tetris.piece.Piece. p/Q 1 1))
(def PieceQ90 (p/make "Q9"))
(def PieceI03 (tetris.piece.Piece. p/I 0 3))
(def PieceZ50 (tetris.piece.Piece. p/Z 5 0))
(def PieceZ51 (tetris.piece.Piece. p/Z 5 1))
(def PieceZ11 (tetris.piece.Piece. p/Z 1 1))
(def PieceZ28 (tetris.piece.Piece. p/Z 2 8))
(def PieceZ1-98 (tetris.piece.Piece. p/Z 1 98))

(def packable-grid
  (tetris.grid.Grid. 3 5 '(() () (0 1 2) () (0 1 2))))

(def packable-grid2
  (tetris.grid.Grid. 3 5 '(() () (0 1 2) (1) (0 1 2))))

(def occupied-grid
  (tetris.grid.Grid. 3 5 '(() () (0 1 2) () (1 2))))

(def fully-occupied-grid
  (tetris.grid.Grid. 3 5 '((0 1 2) (0 1 2) (0 1 2) (0 1 2) (0 1 2))))

(def Q0-grid
  (tetris.grid.Grid. 3 5 '(() () () (0 1) (0 1))))

(def I0-grid
  (tetris.grid.Grid. 10 5 '(() () () (0 1 2 3) ())))

(def another-grid
  (tetris.grid.Grid. 10 10 '(() () () () () () () () (0 1 2 3) (1))))

(def T1-debug-grid
  (tetris.grid.Grid. 10 10 '(() () () () () () () (0 4 5 9) (0 4 5 9) (0 4 5 9) (0 1 3 4 5 6 8 9))))

(def PieceT16 (tetris.piece.Piece. p/T 1 6))

(def empty-grid (gr/make 10 100))
(def small-empty-grid (gr/make 3 5))
