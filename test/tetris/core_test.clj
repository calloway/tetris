(ns tetris.core-test
  (:require [clojure.test :refer :all]
            [tetris.core :refer :all]
            [tetris.tetrisreader :as tr]
            [tetris.grid :as gr]
            [tetris.piece :as p]
            [tetris.game :as g])
  (:require [tetris.test-data :as td])
  (:use [tetris piece-test grid-test game-test tetrisreader-test]))
