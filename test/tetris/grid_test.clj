(ns tetris.grid-test
  (:require [clojure.test :refer :all]
            [tetris.core :refer :all]
            [tetris.tetrisreader :as tr]
            [tetris.grid :as gr]
            [tetris.game :as g]
            [tetris.piece :as p])
    (:require [tetris.test-data :as td]))

(deftest simple-occupied
  (testing "simple occupied")
  (is (true? (gr/occupied? td/occupied-grid 0 2)))
  (is (true? (gr/occupied? td/fully-occupied-grid 0 0)))
  (is (false? (gr/occupied? (gr/make 10 100) 0 0)))
  (is (false? (gr/occupied? td/occupied-grid 0 99))))

(deftest highest-occupied-row-empty-grid
  (testing "highest-occupied-row-empty-grid")
  (is (= 0 (gr/highest-occupied-rowcount td/empty-grid)))
  (is (= 3 (gr/highest-occupied-rowcount td/occupied-grid))))

(deftest simple-fallable? ()
  (testing "if a piece is fallable")
  (is (false? (gr/fallable? td/Q0-grid td/PieceQ11)))
  (is (true? (gr/fallable? td/I0-grid td/PieceI03)))
  (is (true? (gr/fallable? td/empty-grid td/PieceZ50)))
  (is (false? (gr/fallable? td/occupied-grid td/PieceZ11)))
  (is (false? (gr/fallable? td/occupied-grid td/PieceQ01)))
  (is (false? (gr/fallable? td/occupied-grid td/PieceQ00)))
  (is (true? (gr/fallable? td/T1-debug-grid td/PieceT16)))
  (is (false? (gr/fallable? td/empty-grid td/PieceZ1-98))))

(deftest simple-occupy-grid-with-piece
  (testing "simple occupy grid with piece")
  (is  (= '(() () () (0 1) (0 1)) (:occupied (gr/occupy-grid-with-piece td/small-empty-grid td/PieceQ03)))))

(deftest simple-debug-T1
  (testing "simple debugging T1 piece")
  (is  (= 
        '(() () () () () () (1 2 3) (0 2 4 5 9) (0 4 5 9) (0 4 5 9) (0 1 3 4 5 6 8 9))
          (:occupied (gr/occupy-grid-with-piece td/T1-debug-grid td/PieceT16)))))

(deftest packgrid-works
  (testing "packgrid works")
  (is (= '(() () () () ()) (:occupied (gr/pack-grid td/fully-occupied-grid))))
  (is (= '(() () () () (1)) (:occupied (gr/pack-grid td/packable-grid2))))
  (is (= td/Q0-grid (gr/pack-grid td/Q0-grid))))


(deftest simple-occupying-merge
  (testing "simple occupying merge")
  (is (= '((0 1) (1 2) () () (3))
         (gr/occupying-merge '(() () () () (3)) '((0 1) (2 1)))))
  (is (= '((1 3 7) (2))
         (gr/occupying-merge '((1 3) (2)) '((7) ()))))
  (is (= '((1 3 7) (2))
         (gr/occupying-merge '((1 3) (2)) '((7)))))
  (is (= '((1 7))
         (gr/occupying-merge '((1)) '((7)))))
  (is (= '((0 1) (2 1))
         (gr/occupying-merge nil '((0 1) (2 1))))))

