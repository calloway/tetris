(ns tetris.core (:gen-class)
    (:require [tetris.tetrisreader :as tr])
    (:require [tetris.gameptr :as gp])
    (:require [tetris.grid :as gr])    
    (:require [ tetris.ui.console :as c])
    (:require [tetris.ui.gui :as gui]))

(defn -main [& args]
  (if (= 0 (count args))
    (println "usage: tetris.sh input.txt ||| or lein run input.txt")
    (let [gameptr (gp/make (tr/make (first args)) (gr/make gr/MAXCOLS gr/MAXROWS))]
      (do (future (gui/-main gameptr))
          (future (c/-main gameptr))))))
