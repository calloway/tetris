# tetris

Tetris demo in clojure.  Very simple.

 - Problem Statement ![Output](raw/master/TetrisProgrammingExercise.pdf "Problem Statement")
 - Sample Input ![Output](raw/master/input.txt "input.txt")

## Usage

 > lein run input.txt
 > tetris.sh input.txt
 > tetris.bat input.txt
 > java -jar target/tetris-0.1.0-SNAPSHOT-standalone.jar input.txt

## Output

 - see output.txt [Output](raw/master/output.txt "output.txt")
 - screenshot.png ![IMAGE](raw/master/screenshot.png "Screenshot")

## Design

 - grid stores numcols, numrows, and spaces occupied
 - occupied spaces is a nested list so
    '(() () (1 2 3) (0 1 2 3))  , numcols: 4, numrows 4 
    indicates a grid where 
       * columns 1,2,3 on 2nd bottom row is occupied
       * all columns on bottom row are occupied
       * the top 2 rows are empty
  - Piece stores the shape and the leftmost/topmost coordinates
  - A Piece can fall on the grid, but only if none of the spaces it wants to
    occupy are occupied
  - Game stores pieces raw input, current turn i, and the grid
  - tetrisreader knows how to read input files
  - gameptr stores tetrisreader and the game
  - playgame stores logic to play one line/ one entire game of tetris
  - Built 2 User interfaces, (A) console (B) gui
  - Both User interfaces use playgame.clj and bind appropiately to show output

## Testing
 > lein test 
 > lein.bat test

## Lines

280 Lines using http://cloc.sourceforge.net

## Total Time spent

Way more then 8 hours, but a very good opportunity to learn about Clojure

## Bitbucket Private Repo

The source and history of its evolution are on PRIVATE repo on Bitbucket

Copyright © 2013 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
