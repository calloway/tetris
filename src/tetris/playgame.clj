(ns tetris.playgame  
  (:require [tetris.gameptr :as gp])
  (:require [tetris.game :as g])
  (:require [tetris.grid :as gr])
  (:require [tetris.piece :as p]))

(defn- piece-delay [] (. Thread sleep 300))
(defn- game-delay [] (. Thread sleep 2000))

(defn- play-piece [game piece update-grid]
  (let [grid-with-piece (gr/occupy-grid-with-piece (:grid game) piece)       
        game-with-new-grid (tetris.game.Game. (:turns game) (:i game) grid-with-piece)]
    (if (gr/fallable? (:grid game) piece)
      (do
        (update-grid grid-with-piece)
        (piece-delay)
        (play-piece game (p/fall piece) update-grid))
      (do
        (update-grid grid-with-piece)
        game-with-new-grid))))

(defn play-game [gameptr update-grid]
  (if-let [next-piece (g/next-piece (:currentgame gameptr))]
    (let [game-with-piece (play-piece (:currentgame gameptr)
                                         next-piece
                                         update-grid)
          grid-with-piece (:grid game-with-piece)
          packed-grid-with-piece (gr/pack-grid grid-with-piece)
          gptr-next-turn (gp/inc-turn
                          (gp/update-with-grid gameptr packed-grid-with-piece))]
      (do
        (update-grid packed-grid-with-piece)
        (play-game gptr-next-turn update-grid)))
    (do
      (update-grid (:grid (:currentgame gameptr))
                   (gr/highest-occupied-rowcount (:grid (:currentgame gameptr))
                   ))
      (game-delay))))
